FROM rust:1.75-buster

RUN mkdir -p /usr/src/instarust
WORKDIR /usr/src/instarust

COPY ./ /usr/src/instarust

RUN cargo build --release

EXPOSE 3000

ENV RUST_LOG=debug

CMD ./target/release/instarust
